MagesBot = function(){
    this.socket;
    this.myPlayerIndex = 0;
    this.myHp = 100;
    this.enemyHp = 100;
    this.decideIntervalId = null;
};
MagesBot.prototype = {
    createSocket: function(callback) {
        var sockets = new LocalSockets();
        sockets.on('connection', function(socket) {
            callback(socket);
        });
        
        this.socket = sockets.connect();
        
        var _this = this;
        this.socket.on('state', function(data) {
            //console.log('bot state', data);

            var methodName = 'onState' + data.state.charAt(0).toUpperCase() + data.state.slice(1);
            if (typeof _this[methodName] === 'function') {
                _this[methodName](data);
            } else {
                console.log('Bot does not know about method: ' + methodName);
            }
        });
    },
    
    onStateJoined: function(data) {
        this.myPlayerIndex = data.playerIndex;
        
        var _this = this;
        this.decideIntervalId = setInterval(function(){
            _this.decide();
        }, 300);
    }, 
    
    onStateUpdate: function(data) {
        if (this.myPlayerIndex === 1) {
            this.myHp = data.hp1;
            this.enemyHp = data.hp2;
        } else if (this.myPlayerIndex === 2) {
            this.myHp = data.hp2;
            this.enemyHp = data.hp1;
        }
    },
    
    onStateCast: function(data) {
        
    },
    
    onStateEndGame: function(data) {
        if (this.myPlayerIndex === data.winner) {
            console.log('Bot is happy');
        } else {
            console.log('Bot is disappointed');
        }
        clearInterval(this.decideIntervalId);
    },
    
    decide: function() {
        var options = ['fireball', 'lightning'];
        var spellId = options[Math.floor(Math.random()*options.length)];
        this.cast(spellId);
    },
    
    cast: function (spellId) {
        this.socket.emit('action', {action: 'cast', spell: spellId});
    }
};