States.MainMenu = function(game) {
    this.connectFailed = false;
    this.socket;
    
    this.menuControls;
    this.menuBackground;
    this.loadingSpinner;
};
States.MainMenu.prototype = {
    create: function() {
        var groupMenu = this.add.group();
        this.menuBackground = groupMenu.create(0, 0, 'menu_bg');
        this.menuControls = this.add.group();
        var menuPanel = groupMenu.create(this.world.centerX - 269, 170, 'menu_panel');
        menuPanel.alpha = 0.9;
        var buttonPlay = this.add.button(this.world.centerX - 101, 250, 'button_play', this.actionPlay, this, 2, 1, 0);
        this.menuControls.add(menuPanel);
        this.menuControls.add(buttonPlay);
        
        groupMenu.add(this.menuControls);
        
        
        this.loadingSpinner = this.add.sprite(400, 300, 'spinner');
        this.loadingSpinner.anchor.setTo(0.5, 0.5);
        this.loadingSpinner.kill();
    },
    update: function() {
        this.loadingSpinner.angle += 2;
    },
    actionPlay: function() {
        console.log('play');
        this.menuBackground.alpha = 0.3;
        this.menuControls.destroy();
        this.loadingSpinner.revive();
        this.connectToServer();
    },
    connectToServer: function() {
        var wsAdress = 'http://' + document.location.hostname + ':' + globalPort;
        this.socket = io.connect(wsAdress, {reconnection: false});

        var _this = this;
        this.socket.on('state', function(data) {
            if (data.state === 'connected') {
                clearTimeout(loadingTimeoutId);
                _this.onRemoteServerConnected();
            }
        });

        var loadingTimeoutId = setTimeout(function() {
            console.log('Without server');
            _this.connectFailed = true;
            var localServer = new LocalServer();
            _this.socket = localServer.connect();
            _this.onLocalServerConnected();
        }, 1000);
    },
    onRemoteServerConnected: function() {
        var _this = this;

        this.socket.on('state', function(data) {
            if (_this.connectFailed) {
                return;
            }
            if (data.state === 'joined') {
                _this.startGame(data);
            }
        });

        this.socket.emit('action', {action: 'join'});
    },
    onLocalServerConnected: function() {
        var _this = this;

        this.socket.on('state', function(data) {
            if (data.state === 'joined') {
                _this.startGame(data);
            }
        });
        
        this.socket.emit('action', {action: 'join'});
    },
    
    startGame: function(joinedData) {
        this.game.state.states['Game'].socket = this.socket;
        this.game.state.states['Game'].joinedData = joinedData;
        this.game.state.start('Game');
    }
};