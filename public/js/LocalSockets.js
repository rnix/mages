/**
 * Implements interface of Socket.IO.
 * Creates a couple of linked sockets: client-side and server-side.
 * 
 * @returns {LocalSockets}
 */

LocalSockets = function (){
    this.onConnectionCallback = function(){};
    
    this.Socket = function (){
        this.otherSideSocket;
        this.onCallbacks = {};
        this.id = false;
    };
    this.Socket.prototype = {
        emit: function(event, data) {
            this.otherSideSocket.runEventCallbacks(event, data);
        },
        on: function(event, callback) {
            if (typeof this.onCallbacks[event] === 'undefined') {
                this.onCallbacks[event] = [];
            }
            this.onCallbacks[event].push(callback);
        },
        runEventCallbacks: function(event, data) {
            if (typeof this.onCallbacks[event] === 'object') {
                for (var i = 0; i < this.onCallbacks[event].length; i++) {
                    this.onCallbacks[event][i](data);
                }
            }
        },
        setOtherSideSocket: function(otherSideSocket) {
            this.otherSideSocket = otherSideSocket;
        }
    };
};

LocalSockets.prototype = {
    
    connect: function() {
        var clientSocket = new this.Socket();
        var serverSocket = new this.Socket();
        clientSocket.setOtherSideSocket(serverSocket);
        serverSocket.setOtherSideSocket(clientSocket);
        
        var id = Math.floor((1 + Math.random()) * 0x10000);
        clientSocket.id = id;
        serverSocket.id = id;
        
        this.onConnectionCallback(serverSocket);
        this.onConnectionCallback = function() {};
        return clientSocket;
    },
    
    on: function(event, callback) {
        if (event === 'connection') {
            this.onConnectionCallback = callback;
        }
    }
};
