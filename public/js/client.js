(function() {

    var game = new Phaser.Game(800, 600, Phaser.CANVAS, '');
    game.state.add('Boot', States.Boot);
    game.state.add('Preloader', States.Preloader);
    game.state.add('MainMenu', States.MainMenu);
    game.state.add('Game', States.Game);
    game.state.start('Boot');

})();