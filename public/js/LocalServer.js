LocalServer = function() {
    this.sockets = new LocalSockets();
};
LocalServer.prototype = {
    connect: function() {
        this.sockets.on('connection', function(socket) {
            socket.on('action', function(data) {
                if (data.action === 'join') {
                    var magesBot = new MagesBot();
                    magesBot.createSocket(function(botSocket) {
                        var battle = new Battle();
                        setTimeout(function(){
                            battle.start(socket, botSocket);
                        }, 100);
                    });
                }
            });
        });
        return this.sockets.connect();
    },
};
