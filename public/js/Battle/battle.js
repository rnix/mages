Battle = function(){ 
    if (typeof require === 'function') {
        this.spells = require('./spells').spells;
    } else {
        this.spells = exports.spells;
    }
    
    this.gameIsOver = true;
};
Battle.prototype = {
    start: function(socket1, socket2) {
        this.player1 = this.initializePlayer(socket1, 1);
        this.player2 = this.initializePlayer(socket2, 2);

        var _this = this;
        this.player1.on('action', function(data) {
            _this.onInputAction(data, _this.player1);
        });
        this.player2.on('action', function(data) {
            _this.onInputAction(data, _this.player2);
        });
        
        this.player1.emit('state', {
            state: 'joined',
            playerIndex: 1
        });
        this.player2.emit('state', {
            state: 'joined',
            playerIndex: 2
        });
        
        this.gameIsOver = false;
    },
    
    initializePlayer: function(socket, playerIndex) {
        socket.health = 100;
        socket.damage = function(dmg) {
            this.health -= dmg;
        };
        socket.playerIndex = playerIndex;
        return socket;
    },
    
    onInputAction: function(data, player) {
        if (this.gameIsOver) {
            console.log('Game Is Over');
            return;
        }
        var methodName = 'onInputAction' + data.action.charAt(0).toUpperCase() + data.action.slice(1);
        if (typeof this[methodName] === 'function') {
            this[methodName](data, player);
        } else {
            console.error('Unrecognized action: ' + data.action);
        }
    },
    
    onInputActionJoin: function(data, player) {
        
    },
    
    onInputActionCast: function(data, sourcePlayer) {
        var opponent;
        if (this.player1.id === sourcePlayer.id) {
            opponent = this.player2;
        } else if (this.player2.id === sourcePlayer.id) {
            opponent = this.player1;
        }
        if (!opponent) {
            console.error('Opponent not found');
            return;
        }

        if (!this.spells[data.spell]) {
            console.error('Spell not found: ' + data.spell);
            return;
        }

        var spell = new this.spells[data.spell]();
        spell.cast(sourcePlayer, opponent);

        this.emit('state', {
            state: 'cast',
            spell: data.spell,
            sourcePlayer: sourcePlayer.playerIndex
        });

        this.updateClients();
    },
    
    emit: function(event, data) {
        this.player1.emit(event, data);
        this.player2.emit(event, data);
    },
    
    updateClients: function() {
        this.emit('state', {
            state: 'update',
            hp1: this.player1.health,
            hp2: this.player2.health
        });


        if (this.player1.health <= 0) {
            this.emit('state', {
                state: 'endGame',
                winner: 2
            });
            this.gameIsOver = true;
        } else if (this.player2.health <= 0) {
            this.emit('state', {
                state: 'endGame',
                winner: 1
            });
            this.gameIsOver = true;
        }
    },
};