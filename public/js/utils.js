var tagBody = '(?:[^"\'>]|"[^"]*"|\'[^\']*\')*';
var tagOrComment = new RegExp(
    '<(?:'
    // Comment body.
    + '!--(?:(?:-*[^->])*--+|-?)'
    // Special "raw text" elements whose content should be elided.
    + '|script\\b' + tagBody + '>[\\s\\S]*?</script\\s*'
    + '|style\\b' + tagBody + '>[\\s\\S]*?</style\\s*'
    // Regular name
    + '|/?[a-z]'
    + tagBody
    + ')>',
    'gi');

var removeTags = function(html) {
  var oldHtml;
  do {
    oldHtml = html;
    html = html.replace(tagOrComment, '');
  } while (html !== oldHtml);
  return html.replace(/</g, '&lt;');
};

/**
 * Makes nickname from md5 hash
 *
 * @param {String} md5-hash
 * @param {Integer} length of nickname
 * @returns {String} nickname
 */
var generateNick = function (hash, wordLength) {

    var adLetters = {
        '19': 'p',
        '18': 'j',
        '17': 'w',
        '16': 'x',
        '15': 'z',
        '14': 'l',
        '13': 'k',
        '12': 'h',
        '11': 'g',
        '10': 'q',
        '9': 's',
        '8': 'r',
        '7': 'v',
        '6': 'n',
        '5': 'm',
        '4': 'i',
        '3': 'o',
        '2': 'u',
        '1': 'y',
        '0': 't',
    };

    hash = hash.toLowerCase();

    //this is optional
    if (hash.length) {
      hash = md5('--prevent--attempts--' + hash + '--to--encode-the-same-nick');
    }

    for (var l in adLetters) {
        hash = hash.replace(new RegExp(l, 'g'), adLetters[l]);
    }

    var vowels = ['a', 'e', 'i', 'o', 'u', 'y'];

    var arrStr = hash.split('');
    var wordArr = [];
    var letter;
    var needVowel = false;
    var i, w;

    // rotation of vowels and consonants
    for (w = 0; w < wordLength; w++) {
        for (i = w; i < arrStr.length; i++) {
            letter = arrStr[i];
            if ( needVowel && (vowels.indexOf(letter) > -1) ) {
                wordArr.push(letter);
                needVowel = false;
                break;
            } else if ( !needVowel && !(vowels.indexOf(letter) > -1) ) {
                wordArr.push(letter);
                needVowel = true;
                break;
            }
        }
    }

    // add random int to word if didn't find enought letters
    if (wordArr.length < wordLength) {
        for (i = wordArr.length; i < wordLength; i++) {
            wordArr.push(Math.floor((Math.random() * 10)));
        }
    }

    var word = wordArr.join('');
    return word;
};

var md5 = function (string) {
  var crypto = require('crypto');
  var hash = crypto.createHash('md5').update(string).digest('hex');
  return hash;
};


var ucfirst = function ( str ) {
  var f = str.charAt(0).toUpperCase();
  return f + str.substr(1, str.length-1);
};

var guid = (function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return function() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
  };
})();

if (typeof exports === 'undefined'){
  exports = {};
}

if (typeof exports.utils === 'undefined'){
  exports.utils = {};
}

exports.utils.ucfirst = ucfirst;
exports.utils.removeTags = removeTags;
exports.utils.generateNick = generateNick;
exports.utils.guid = guid;
