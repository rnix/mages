var States = {};
States.Boot = function(game) {
};
States.Boot.prototype = {
    preload: function() {
        this.load.image('menu_bg', 'assets/menu_bg.png');
        this.load.image('preloaderBar', 'assets/loader_bar.png');
    },
    create: function() {
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.maxWidth = 800;
        this.scale.maxHeight = 600;
        this.scale.setScreenSize(true);
                
        this.game.state.start('Preloader');
    }
};