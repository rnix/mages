var AbstractSpell = Class.extend({

  init: function(game) {
    this.resourceId = null;
    this.offsetX = 0;
    this.offsetY = 0;
    this.rotation = 0;
    this.game = game;
    this.isActive = true;
    this.sprite = false;
    this.playerAnimateDuration = 100;
  },

  onUpdate: function() {

  },

  cast: function(sourcePlayer, targetPlayer) {
    this.sourcePlayer = sourcePlayer;
    this.targetPlayer = targetPlayer;
    this.draw();
  },

  draw: function() {
    var spellPos = this.sourcePlayer.getSpellPos();

    var xPos = spellPos.x + this.offsetX;
    if (this.sourcePlayer.index === 2) {
      xPos = spellPos.x - this.offsetX;
    }

    this.sprite = this.game.add.sprite(xPos, spellPos.y + this.offsetY, this.resourceId);

    if (this.sourcePlayer.index === 2) {
      this.sprite.scale.x *= -1;
    }

    this.animate();
  },

  animate: function() {
    this.sprite.animations.add('go');
    this.sprite.animations.play('go', 25, true);
  },

  checkOverlap: function(spriteA, spriteB) {
    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();
    return Phaser.Rectangle.intersects(boundsA, boundsB);
  }

});

/* */

var LightningSpell = AbstractSpell.extend({

  init: function(game) {
      this._super(game);
      this.playerAnimateDuration = 400;
  },

  draw: function() {
    var spellPos = this.sourcePlayer.getSpellPos();
    
    var xPos = spellPos.x + 18;
    if (this.sourcePlayer.index === 2) {
      xPos = spellPos.x - 18;
    }
    
    this.sprite = this.game.add.sprite(xPos, spellPos.y + 88, 'lightning');
    this.sprite.rotation = -Math.PI / 2;

    if (this.sourcePlayer.index === 2) {
      this.sprite.scale.y *= -1;
    }

   
    this.sprite.animations.add('go');
    this.sprite.animations.play('go', 25);

    this.sprite.events.onAnimationComplete.add(function() {
        this.sprite.kill();
        this.isActive = false;
    }, this);

  }

});

/* */

var FireballSpell = AbstractSpell.extend({

  init: function(game) {
    this._super(game);
    this.resourceId = 'fireball';
    this.offsetY = -72;
  },

  animate: function() {
    this._super();
    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.body.velocity.x = 1000;
    if (this.sourcePlayer.index === 2) {
      this.sprite.body.velocity.x *= -1;
    }
  },

  onUpdate: function() {
    if (this.sprite && Phaser.Point.distance({x: this.targetPlayer.sprite.x, y:0}, {x: this.sprite.x, y: 0}) < 60) {
      this.sprite.kill();
      this.isActive = false;
    }
  },

});
