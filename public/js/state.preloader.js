States.Preloader = function(game) {
};
States.Preloader.prototype = {
    preload: function() {
        this.game.stage.backgroundColor = '#16181a';
        this.add.sprite(0, 0, 'menu_bg');
        this.preloadBar = this.add.sprite(145, this.world.height - 165, 'preloaderBar');
        this.load.setPreloadSprite(this.preloadBar);


        this.load.image('menu_bg', 'assets/menu_bg.png');
        this.load.image('menu_panel', 'assets/menu_panel.png');
        this.load.image('button_play', 'assets/button_play.png');
        this.load.image('spinner', 'assets/spinner.png');
        this.load.image('black', 'assets/black.png');
        this.load.image('defeat', 'assets/defeat.png');
        this.load.image('victory', 'assets/victory.png');

        this.load.image('bg', 'assets/forest.png');
        this.load.image('platform', 'assets/platform.png');
        this.load.spritesheet('mage', 'assets/mage.png', 130, 110);
        this.load.image('hb_bar', 'assets/blood_red_bar.png');
        this.load.image('hb_health1', 'assets/blood_red_bar_health.png');
        this.load.image('hb_health2', 'assets/blood_red_bar_health.png');
        this.load.image('icon_fireball', 'assets/icons/fireball-red-1.png');
        this.load.image('icon_lightning', 'assets/icons/lightning-blue-1.png');
        this.load.image('icon_frame_red', 'assets/icons/frame-9-red.png');
        this.load.image('icon_frame_blue', 'assets/icons/frame-7-blue.png');
        this.load.spritesheet('lightning', 'assets/lightning.png', 196, 534);
        this.load.spritesheet('fireball', 'assets/fireball.png', 64, 64);

    },
    create: function() {
        this.game.state.start('MainMenu');
    }
};