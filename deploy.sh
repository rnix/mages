#!/bin/bash
#
# deploy project
#
#

TARG="/var/www/mages"
HGSOURCE="ssh://hg@bitbucket.org/rnix/mages"

cd "$TARG" || exit 1

echo hg pull -u "$HGSOURCE"
hg pull -u "$HGSOURCE"

forever restart /var/www/mages/app.js

echo OK!

