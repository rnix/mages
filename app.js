
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var ejs = require('ejs'); ejs.open = '<?'; ejs.close = '?>';

var app = express();


app.set('port', process.env.PORT || 3002);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(app.router);
//app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

var server = http.createServer(app);

var io = require('socket.io').listen(server);

var serverModule = require('./server/server');
var appServer = new serverModule.AppServer(io.sockets);
appServer.start();


server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});