
/*
 * GET home page.
 */

exports.index = function(req, res){
  var env = req.app.get('env');
  var port = req.app.get('port');
  var uid = '';
  if (typeof req.query.uid === 'string' && req.query.uid.length > 5) {
      uid = req.query.uid;
  } else {
      uid = req.headers['user-agent'] + req.ip;
  }
  uid = uid.replace(/[^\w]/g, '');

  res.render('index', { title: 'Express', 'env': env, 'port': port, 'uid': uid });
};