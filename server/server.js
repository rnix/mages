function AppServer(sockets){
  this.sockets = sockets;
  this.player1 = false;
  this.player2 = false;

  this.utils = require('./utils').utils;
  this.spells = require('./spells').spells;
}

AppServer.prototype.start = function() {
  var _this = this;

  this.sockets.on('connection', function (socket) {
      
      if (!_this.player1) {
          _this.player1 = _this.initializePlayer(socket, 1);
      } else if (!_this.player2) {
          _this.player2 = _this.initializePlayer(socket, 2);
      } else {
          console.log('Third player');
      }

    socket.emit('state', {state: 'connected'});


    socket.on('action', function(data) {
      try {
        if (data.action === 'join') {

          socket.emit('state', {
              state: 'joined',
              playerIndex: socket.playerIndex
          });

        } else if (data.action === 'send') {
          if (data.text.length > 300) {
            data.text = data.text.substring(0, 300) + '...';
          }
          var now = _this.getNow();
          var message = {
            'time': _this.getTimestamp(now),
            'text': _this.utils.removeTags(data.text),
            'author': socket.nick
          };

          _this.sockets.in('joined-room').emit('state', {
            'state': 'message',
            'message': message
          });

        } else if (data.action === 'cast' && data.spell) {
            var opponent;
            if (_this.player1.id === socket.id) {
                opponent = _this.player2;
            } else if (_this.player2.id === socket.id) {
                opponent = _this.player1;
            }
            if (!opponent) {
                console.log('Opponent not found');
                return;
            }

            if (!_this.spells[data.spell]) {
              console.log('Spell not found: ' + data.spell);
              return;
            }
            
            var spell = new _this.spells[data.spell]();
            spell.cast(socket, opponent);
            
            _this.sockets.emit('state', {
                state: 'cast',
                spell: data.spell,
                sourcePlayer: socket.playerIndex
            });
            
            _this.updateClients();
        }
        
        
      } catch (e) {
        console.log(e.stack);
      }
    });

    socket.on('disconnect', function() {
      if (socket.playerIndex) {
          if (socket.playerIndex === 1) {
              _this.player1 = false;
          } else if (socket.playerIndex === 2) {
              _this.player2 = false;
          }
      }
    });

  });


  //setInterval(function(){
  //  _this.usersUpdateTick();
  //}, 100);

};

AppServer.prototype.initializePlayer = function(socket, playerIndex) {
    socket.health = 100;
    socket.damage = function(dmg) {
        this.health -= dmg;
    };
    socket.playerIndex = playerIndex;
    return socket;
};

AppServer.prototype.updateClients = function(){

    this.sockets.emit('state', {
        state: 'update',
        hp1: this.player1.health,
        hp2: this.player2.health
    });
    
    
    if (this.player1.health <= 0) {
        this.sockets.emit('state', {
            state: 'endGame',
            winner: 2
        });
        this.player1 = false;
        this.player2 = false;
    } else if (this.player2.health <= 0) {
        this.sockets.emit('state', {
            state: 'endGame',
            winner: 1
        });
        this.player1 = false;
        this.player2 = false;
    } 

};

AppServer.prototype.filterNick = function(nick) {
  nick = nick.replace(/[\s]/g, '');
  nick = this.utils.removeTags(nick);
  return nick;
};

AppServer.prototype.checkNick = function(nick) {
  if (nick.length > 2 && nick.length <= 12) {
    return true;
  }
  return false;
};

AppServer.prototype.getNow = function() {
  var now = new Date();
  var kemerovoOffset = 7 * 3600000;
  var serverOffset = now.getTimezoneOffset() * 60000;
  var kemerovoTime = new Date(now.getTime() + kemerovoOffset + serverOffset);
  return kemerovoTime;
};

AppServer.prototype.getTimestamp = function(date) {
  var ts = ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
  return ts;
};

AppServer.prototype.usersUpdateTick = function() {
  this.updateClients();
};


exports.AppServer = AppServer;
