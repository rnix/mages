require('./inheritance');

var AbstractSpell = Class.extend({

  delayedEffect: function(timeout, effect) {
    setTimeout(effect, timeout);
  },

  delayedDamage: function(delay, targetPlayer, damage) {
    this.delayedEffect(delay, function(){
        targetPlayer.damage(damage);
    });
  }

});

var Fireball = AbstractSpell.extend({

  duration: 400,
  damage: 5,

  cast: function(sourcePlayer, targetPlayer) {
    this.sourcePlayer = sourcePlayer;
    this.targetPlayer = targetPlayer;

    this.delayedDamage(this.duration, targetPlayer, this.damage);
  },

});

var Lightning = AbstractSpell.extend({

  damage: 5,

  cast: function(sourcePlayer, targetPlayer) {
    this.sourcePlayer = sourcePlayer;
    this.targetPlayer = targetPlayer;

    targetPlayer.damage(this.damage);
  },

});

if (typeof exports.spells === 'undefined'){
  exports.spells = {};
}

exports.spells.fireball = Fireball;
exports.spells.lightning = Lightning;